var razsor = {
    definePackage : function(name) {
        var currentPackage = window;
        var currentPackageString = "";
        var packages = name.split(".");
        var packageName = packages[packages.length - 1];
        
        for (var i = 0, len = packages.length - 1; i < len; i++) {
            currentPackageString += i == 0 ? packages[i] : "." + packages[i];
            
            if (!currentPackage[packages[i]]) {
                throw new razsor.RazorError("Cannot create \"" + name + "\" because package \"" + currentPackageString + "\" does not exist.");
            }
            currentPackage = currentPackage[packages[i]];
        }

        if (currentPackage[packageName]) {
            throw new razsor.RazorError("Cannot create \"" + name + "\" because \"" + packageName + "\" already exists on package \"" + currentPackageString + "\"");
        } 
        else {
            currentPackage[packageName] = {};
            currentPackage[packageName].toString = function() { return name };
        }
    },


    extend : function(baseObj, obj, overwrites) {
        // Test used to see if super is called in the method. No need to wrap the function if it's not
        var superTest = /xyz/.test(function(){xyz;}) ? /\b_super\b/ : /.*/
        for (var item in obj) {
            (function(item) {
                // See if we need to wrap thr base function
                if (!overwrites && typeof obj[item] == "function" && superTest.test(obj[item])) {
                    // Make sure we're not overwriting a var with a function
                    if (baseObj[item] && typeof baseObj[item] != "function") {
                        throw new razsor.RazorError("Can't override existing property on the base object with a function");
                    }
                    var _super = baseObj[item];
                    baseObj[item] = function() {
                        var tmp = this._super;
                        this._super = _super;

                        try {
                            var ret = obj[item].apply(this, arguments);
                        }
                        catch(e) {
                            throw e;
                        }
                        finally {
                            // make sure we always clean up
                            this._super = tmp;
                            if (!this._super) {
                                delete this._super;
                            }
                        }

                        return ret;
                    };
                }
                else {
                    //...otherwise just copy the function directly
                    baseObj[item] = obj[item];
                }
            })(item);
        }
    },

    defineClass : function(props) {
        var _className = props._className,
            _package = props._package,
            _class = props._class,
            _static = props._static,
            _abstract = props._abstract,
            _implements = props._implements,
            _extends = props._extends;

        // error checking can be removed for production
        if (!_className) {
            throw new razsor.RazorError("No class name specified");
        }
        if (!_package) {
            throw new razsor.RazorError("Package does not exist or package declaration missing for class " + _className);
        }
        if (_package[_className]) {
            throw new razsor.RazorError(_className + " already exists in package " + _package.toString());
        }
        if (!_class || !(_class.init || _class._init)) {
            throw new razsor.RazorError("No class definition or no init constructor found on class " + _className);
        }
        if ("_extends" in props) {
            // if we extend a base class make sure the base constructor is caled
            if (!_extends) {
                throw new razsor.RazorError("_super declaration for class " + _className + " is undefined");
            }
            var superTest = /xyz/.test(function(){xyz;}) ? /\b_super\b/ : /.*/;
            if (!superTest.test(_class.init)) {
                throw new razsor.RazorError("No call to this._super() found in constructor of class " + _className);
            }
        }

        var Class = function() { 
            // Call the constructor
            this.init.apply(this, arguments);
        };

        if (_extends) {
            var F = function() {};
            F.prototype = _extends.prototype;
            Class.prototype = new F();
            Class.prototype.constructor = Class;
            Class.prototype.__super = _extends;
        }
        // The class shouldn't use its own package and class name directly in the code as this makes it harder to rename/move later
        Class.prototype.__package = _package;
        Class.prototype.__className = _className;

        razsor.extend(Class, _static, true);
        razsor.extend(Class.prototype, _class); 

        // Abstract and implements checking can be removed for production
        //Ensure somehting marked as abstract doesn't already have a concrete implementation
        if (_abstract) {
            for (var item in _abstract.methods) {
                var methodName = _abstract.methods[item];
                if (Class[methodName] || Class.prototype[methodName]) {
                    throw new razsor.RazorError("Concrete implementation for " + methodName + " found in abstract class " + _className + 
                            (_extends ? " or in its inheritance tree." : ""));
                }
            }
        }
        
        //Wrap the constructor to force checking abstract and implements during class creation
        if (_abstract || _implements) {
            var init = Class.prototype.init;
            Class.prototype.init = function() {
                if (_abstract) {
                    razsor.ensureImplements(this, _abstract);
                }
                if (_implements) {
                    for (var i = 0; i < _implements.length; i++) {
                        razsor.ensureImplements(this, _implements[i]);
                    }
                }
                
                init.apply(this, arguments);
            };
        }

        _package[_className] = Class;
    },  

    Interface : function(name, methods) {
        this.name = name;
        var error;

        // Interface Names
        var regExp = /^[A-Z]{1}[\w]*$/;
        if (arguments.length != 2) {
            throw new razsor.RazorError("Interface constructor called with " + arguments.length + " arguments, but expected exactly 2.");
        } 
        if (typeof name != "string" || !regExp.test(name)) {
            throw new razsor.RazorError("Argument 'name' must be a valid string representing a JavaScript identifier. name=" + name);
        } 
        if (!(methods instanceof Array)) {
            throw new razsor.RazorError("Argument 'methods' must be an array. typeof methods=" + typeof methods);
        } 
        if (!methods.length) {
            throw new razsor.RazorError("At least 1 method must be defined in Methods");
        }

        // Method Names
        this.name = name;
        this.methods = [];
        regExp = /^[a-zA-Z$_]{1}[\w\$]*$/
        for (var i = 0, len = methods.length; i < len; i++) {
            if (typeof methods[i] !== 'string' || !regExp.test(methods[i])) {
                throw new razsor.RazorError("Interface method names must be strings representing valid JavaScript identifiers. methods[" + i + "]=" + methods[i]);
            }
            this.methods.push(methods[i]);
        }
    },


    ensureImplements : function(object, interfaceObj) {
        var error;

        if (arguments.length < 2) {
            throw new razsor.RazorError("ensureImplements called with " + arguments.length + " arguments, but expected at least 2.");
        }

        for (var i = 1, len = arguments.length; i < len; i++) {
            interfaceObj = arguments[i];
            if (interfaceObj.constructor !== razsor.Interface) {
                throw new razsor.RazorError("ensureImplements expects arguments two and above to be instances of razsor.Interface.");
            }

            for (var j = 0, methodsLen = interfaceObj.methods.length; j < methodsLen; j++) {
                var method = interfaceObj.methods[j];
                if (!object[method] || typeof object[method] !== 'function') {
                    throw new razsor.RazorError("Object does not implement the " +interfaceObj.name + " interface correctly: Method " + method + 
                            " was not found. This is either because the class does not define the methods in the interface; or you are calling 'new' " +
                            " on an abstract class; or you are calling 'new' on a class that does not provide concrete implementations for each of the" +
                            " abstract methods in its superclass.");                    
                }
            }
        }
    },
    
    //supports late binding incase the function is later wrapped
    //usage: bind("method", context) //late bind, where context is typically 'this'
    //       bind(this.method, this) //dynamic bind 
    bind : function(source, target) {
        return function() {
            if (typeof source == "string") {
                return target[source].apply(target, arguments);
            }
            else {
                return source.apply(target, arguments);
            }
        };
    },
    
    //Helps by adding a stack trace
    RazorError : function(msg) {
        var e = new Error();
        e.message = e.stack || e.stackTrace || msg;
        return e;
    }
};